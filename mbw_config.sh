#!/bin/sh

STEAM_RT="${HOME}/.local/share/Steam/ubuntu12_32/steam-runtime/i386"
#PROGRAM_DIRECTORY="`dirname "$0"`"
PROGRAM_DIRECTORY="$HOME/.local/share/Steam/steamapps/common/MountBlade Warband"
export LD_LIBRARY_PATH="${STEAM_RT}/lib/i386-linux-gnu:${STEAM_RT}/usr/lib/i386-linux-gnu:${PROGRAM_DIRECTORY}"

cd "$PROGRAM_DIRECTORY"
"$PROGRAM_DIRECTORY/mbw_config_linux" "$@"
