#!/bin/bash

# Получение информации о состоянии батареи
# Зависимости: cat, bc

BAT_PATH="/sys/class/power_supply/BAT0"
FULL_CHARGE=$(cat $BAT_PATH/charge_full_design)
CURRENT_CHARGE=$(cat $BAT_PATH/charge_now)
PERCENT_CHARGE=$(echo "($CURRENT_CHARGE*100)/$FULL_CHARGE" | bc)
STATUS=$(cat $BAT_PATH/status)
CURRENT_VOLTAGE_ABS=$(cat $BAT_PATH/voltage_now)
CURRENT_VOLTAGE=$(echo "$CURRENT_VOLTAGE_ABS/1000000" | bc)

echo " Battery"
echo "========="
echo "$PERCENT_CHARGE% CHRG"
echo "$CURRENT_VOLTAGE  Volt"
echo "$STATUS"
