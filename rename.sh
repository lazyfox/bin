#!/bin/bash
# Скрипт заменяет часть имени или все имя по шаблону всех файлов
# в текущей директории, удовлетворящих условию
# Интерфейс: rename.sh pattern replacement

PATTERN=$1          #pattern, та часть имени, которую надо заменить
REPLACEMENT=$2      #replacement, то, на что будем заменять

for FILE_NAME in *$PATTERN*
do
    #Проверим, является ли FILE_NAME файлом, каталоги не будем переименовывать
    if [ -f $FILE_NAME ]
    then
        NEW_FILE_NAME=`echo $FILE_NAME | sed s/$PATTERN/$REPLACEMENT/`
        mv $FILE_NAME $NEW_FILE_NAME
    fi
done
