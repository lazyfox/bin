#!/bin/bash
#
NAME=`basename $0`      # Считывание имени, под которым запущен скрипт
DATE=`date +%Y-%m-%d`
case $NAME in
    dobackup.sh)
        FILE=dotfiles_$DATE.zip
    CONFIG_FILE=$HOME/bin/dotfiles_list.txt;;
#prbackup.sh)
    #FILE=program_$DATE.zip
    #CONFIG_FILE=$HOME/bin/program_list.txt;;
nobackup.sh)
    FILE=notes_$DATE.zip
    CONFIG_FILE=$HOME/bin/notes_list.txt;;
wikibackup.sh)
    FILE=wiki_$DATE.zip
    CONFIG_FILE=$HOME/bin/wiki_list.txt;;
esac
#
DESTINATION=$HOME/archive/$FILE
#Если архив уже существует, его надо удалить
if [ -f $DESTINATION ]
then
	rm $DESTINATION
	echo "The previous backup file is removed."
fi
#
#
#Существует ли конфигурационный файл
#
cd $HOME
#
if [ -f $CONFIG_FILE ]
then
	echo	#просто продолжить работу
else
	echo
	echo "$CONFIG_FILE does not exist."
	echo
	exit	#выйти из скрипта
fi
#
# Сформировать список всех имен файлов,
# подлежащих разервному копированию.
#
FILE_NO=1
exec < $CONFIG_FILE # Задать вместо стандартного ввода
					# имя файла конфигурации
read FILE_NAME		# Считать в переменную первую запись
#
while [ $? -eq 0 ]	# $? - переменная, содержащая параметр выхода команды
					# в случае успеха, он равен нулю
					# $? -eq 0 - равен ли он нулю
					# Сформировать список файлаов, подлежащих
					# резервному копированию
do
	# Убедится в том, что файл или каталог существует.
	#
	if [ -f $FILE_NAME -o -d $FILE_NAME ]
	then
		# Если файл существует, добавить его имя к списку
		FILE_LIST="$FILE_LIST $FILE_NAME"
	else
		# Если файл не существует, сформировать предупреждающее
		# сообщение
		echo "$FILE_NAME does not exit."
		echo "Obviously, it will not included in this archive."
		echo "It is listed on line $FILE_NO of this config file."
		echo "Continuing to build archive file..."
	fi
#
	FILE_NO=$[$FILE_NO + 1]	# Увеличить номер строки/номер файла
							# на единицу.
	read FILE_NAME			# Считать следующую запись.
done
#
#################
#
# Создать резервную копию файлов и произвести сжатие архива
#
#tar -czf $DESTINATION $FILE_LIST 2>/dev/null
7z a -tzip $DESTINATION $FILE_LIST &>/dev/null
# Копирование архива на yandex-disk
cp -v $DESTINATION Yandex.Disk/
# Просмотр содержимого архива
7z l -tzip $DESTINATION | less
