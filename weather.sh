#!/bin/bash
# Извлечение сведений о погоде в Москве
URL="http://xml.weather.yahoo.com/forecastrss?p=RSXX0063&u=c"
TMPFILE=`mktemp tmpXXXXXX`

# Запишем содержимое url во временный файл
curl -s $URL -o $TMPFILE

location=`cat $TMPFILE | sed -n '/weather:location/p'`
city=`echo $location | grep -o 'city="[^"]*"' | grep -o '"[^"]*"'`
city=`echo $city | grep -o '[^"]*'`

country=`echo $location | grep -o 'country="[^"]*"' | grep -o '"[^"]*"'`
country=`echo $country | grep -o '[^"]*'`

#-----------------------------------------------------------------------------
# Current condition
condition=`cat $TMPFILE | sed -n '/weather:condition/p'`
#echo $condition

text=`echo $condition | grep -o 'text="[^"]*"' | grep -o '"[^"]*"'`
text=`echo $text | grep -o '[^"]*'`
temperature=`echo $condition | grep -o 'temp="[^"]*"' | grep -o '"[^"]*"'`
temperature=`echo $temperature | grep -o '[^"]*'`
date=`echo $condition | grep -o 'date="[^"]*"' | grep -o '"[^"]*"'`
date=`echo $date | grep -o '[^"]*'`

echo -e "\033[01;35mYahoo! Weather - $city, $country"
echo -e "\033[01;36mCondition for $city, $country at $date"
echo -e "\033[0m$text, $temperature °C\n"
#-----------------------------------------------------------------------------
# Forecast
echo -e "\033[01;36mForecast:\033[0m"
cat $TMPFILE | grep -A 6 'Forecast:' | sed '/Forecast/d' | sed 's/<[^>]*>//g'
# последняя команда служит для удаления дескрипторов HTML
#-----------------------------------------------------------------------------
# Курсы валют
# Курс доллара
TMPFILE_USD=`mktemp tmpXXXXXX`
TMPFILE_EUR=`mktemp tmpXXXXXX`
URL_VAL="http://www.cbr.ru/scripts/XML_daily.asp?d=0"

curl -s $URL_VAL | iconv -f WINDOWS-1251 -t UTF-8 > $TMPFILE
cat $TMPFILE | grep -A 3 'USD' > $TMPFILE_USD
cat $TMPFILE | grep -A 3 'EUR' > $TMPFILE_EUR


#echo "---------------"
#cat $TMPFILE_USD
#echo "---------------"
#cat $TMPFILE_EUR
#echo "---------------"

val_data=`cat $TMPFILE | grep -o 'Date="[^"]*"' | grep -o '"[^"]*"'`
val_data=`echo $val_data | grep -o '[^"]*'`

val_usd=`cat $TMPFILE_USD | grep 'Value' | sed 's/<[^>]*>//g'`
val_eur=`cat $TMPFILE_EUR | grep 'Value' | sed 's/<[^>]*>//g'`

echo -e "\033[01;35mCurrency rate at $val_data"
echo -e "\033[01;35mUSD: \033[01;36m$val_usd"
echo -e "\033[01;35mEUR: \033[01;36m$val_eur"
#-----------------------------------------------------------------------------
rm -f $TMPFILE
rm -f $TMPFILE_USD
rm -f $TMPFILE_EUR
