#!/bin/bash
#
# simple console util for translation text to any language
# using Yandex Translate API
# use: trans.sh call me, baby
# will return translation of a phrase to ru or en language
# more example at http://api.yandex.ru/translate/
# Requires API key
#

input=${@//[&]/%26}
key=`cat $HOME/.key_api.txt`
link='https://translate.yandex.net/api/v1.5/tr.json/translate?'

function translate() {
    translated1=`curl -s "${link}key=$key&lang=$1&text=$input"`
    translated=`echo $translated1 | awk -F'"' {' print $10 '}`
}

translate "ru"
if [ "$translated" == "$input" ]; then
    translate "en"
fi

echo $translated
