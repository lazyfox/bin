#!/bin/bash
#Выводит в терминал все 256 цветов
for ((x=0; x<=255; x++))
do
	echo -e "${x}:\033[38;5;${x}mcolor${x}\033[000m \033[48;5;${x}mcolor${x}\033[000m"
done
