#!/bin/bash
# Сортирует файл истории браузера и удаляет
# повторяющиеся строки
cd $HOME
FILE_HISTORY=$HOME/.local/share/uzbl/history
FILE_COOKIES=$HOME/.local/share/uzbl/cookies.txt
FILE_SESSION=$HOME/.local/share/uzbl/session-cookies.txt

# если опция -d, то очистить историю и кэш
if [ -n $1 ]
then
    case "$1" in
    -d)
        echo > $FILE_COOKIES
        echo > $FILE_SESSION
        echo > $FILE_HISTORY
        echo "Cookies and history are deleted";;
    *)
        # sort -k3 параметр k указывает, по какому полю сортировать
        # uniq удаляет одинаковые вхождения
        # uniq -f2 Игнорировать при сравнении первые 2 поля каждой строки ввода
        # Для удаления строк с одинаковыми полями функцией uniq, эти строки
        # должны идти подряд. Для этого и сделана первая сортировка
        sort -k3 $FILE_HISTORY | uniq -f2 | sort > history.new
        cp history.new $FILE_HISTORY
        rm history.new
        echo "History is cleared";;
    esac
fi
