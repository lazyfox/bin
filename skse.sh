#!/bin/bash
STEAM_PREFIX="$HOME/.wine_steam"
cd "$STEAM_PREFIX/drive_c/Program Files/Steam/steamapps/common/Skyrim"
# optirun -b primus env WINEPREFIX=$STEAM_PREFIX WINEDEBUG=fps wine skse_loader.exe 2>&1 | tee /dev/stderr | grep --line-buffered "^trace:fps:" | osd_cat -o 10 -c green -s 1 -l2
env WINEPREFIX=$STEAM_PREFIX wine skse_loader.exe 2>&1
