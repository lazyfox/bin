#!/bin/sh

STEAM_RT="${HOME}/.local/share/Steam/ubuntu12_32/steam-runtime/i386"
PROGRAM_DIRECTORY="$HOME/.local/share/Steam/steamapps/common/MountBlade Warband"

LD_LIBRARY_PATH="${STEAM_RT}/lib/i386-linux-gnu:${STEAM_RT}/usr/lib/i386-linux-gnu"
LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${PROGRAM_DIRECTORY}"
export LD_LIBRARY_PATH

cd "$PROGRAM_DIRECTORY"
# vblank_mode=0 optirun -b primus "$PROGRAM_DIRECTORY/mb_warband_linux" "$@"
"$PROGRAM_DIRECTORY/mb_warband_linux" "$@"
